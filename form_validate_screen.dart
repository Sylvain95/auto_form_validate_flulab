import 'package:flutter/material.dart';
import 'package:get/get.dart';

class FormValidateScreen extends StatelessWidget {
  const FormValidateScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Form validate"),
        centerTitle: true,
        backgroundColor: Colors.pink,
      ),
      body: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              margin: const EdgeInsets.all(15),
              child: TextFormField(
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderSide: BorderSide.none,
                        borderRadius: BorderRadius.circular(20)),
                    fillColor: Colors.grey.withOpacity(0.3),
                    hintText: "Text",
                    filled: true,
                    focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide:
                            const BorderSide(color: Colors.green, width: 2)),
                    focusedErrorBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide:
                            const BorderSide(color: Colors.pink, width: 2)),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide.none,
                        borderRadius: BorderRadius.circular(10))),
                validator: (value) {
                  if (!GetUtils.isEmail(value!)) {
                    return "Invalid email";
                  } else {
                    return null;
                  }
                },
              ),
            ),
            Container(
              margin: const EdgeInsets.all(15),
              child: TextFormField(
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderSide: BorderSide.none,
                        borderRadius: BorderRadius.circular(20)),
                    fillColor: Colors.grey.withOpacity(0.3),
                    hintText: "Pass word",
                    filled: true,
                    focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide:
                            const BorderSide(color: Colors.green, width: 2)),
                    focusedErrorBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide:
                            const BorderSide(color: Colors.pink, width: 2)),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide.none,
                        borderRadius: BorderRadius.circular(10))),
                validator: (value) {
                  if (!GetUtils.isLengthGreaterThan(value, 5)) {
                    return "Min 5 chars";
                  } else if (!GetUtils.hasCapitalletter(value!)) {
                    return "No capitalize";
                  } else if (!RegExp(r'^(?=.*[a-z])').hasMatch(value)) {
                    return "Minuscule Minuscule";
                  } else if (!RegExp(r'^(?=.*?[0-9])').hasMatch(value)) {
                    return "Chiffre Chiffre";
                  } else if (!RegExp(r'^(?=.*?[!@#\$&*~])').hasMatch(value)) {
                    return "SpecialChar SpecialChar";
                  } else if (!RegExp(r'^.{8,}').hasMatch(value)) {
                    return "8Char 8Char";
                  } else {
                    return null;
                  }
                },
              ),
            ),
          ]),
    );
  }
}
